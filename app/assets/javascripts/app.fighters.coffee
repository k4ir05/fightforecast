App.Fighters =
  autocompleteSelect: (id, num)->
    $("#fighter#{num}-id").val(id)
    $("#fighter#{num}-edit-link").attr('href', "/fighters/#{id}/edit").attr('data-remote', true)

  modalSelect: (fighter)->
    fighter = JSON.parse(fighter)
    $input = $('.fighter.input-group.active')
    $input.find('.fighter-id').val(fighter.id)
    $input.find('.fighter-search').val(fighter.name)
    $input.find('[data-behavior~=edit-fighter]').attr('href', "/fighters/#{fighter.id}/edit").attr('data-remote', true)
    App.hideModal()

$(document).on 'click', '[data-behavior~=new-fighter], [data-behavior~=edit-fighter]', (event)->
  event.preventDefault()
  $('.fighter.input-group.active').removeClass('active')
  $(this).closest('.fighter.input-group').addClass('active')