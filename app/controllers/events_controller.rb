class EventsController < ApplicationController
  before_action :set_event, only: %i[edit update destroy]
  decorates_assigned :events, :event, :forecasts

  def index
    @events = Event.all
  end

  def show
    @event = Event.includes(fights: :forecasts).find(params[:id])
    set_forecasts
  end

  def new
    @event = Event.new
  end

  def edit
  end

  def create
    @event = Event.new(event_params.merge(author_id: current_user.id))

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
    end
  end

  private

  def set_event
    @event = Event.find(params[:id])
  end

  def set_forecasts
    @forecasts = @event.fights.collect do |fight|
      fight.forecasts.by(current_user).first_or_initialize
    end
  end

  def event_params
    params.require(:event).permit(:name, :time, :description, :kind)
  end

end
