class FightersController < ApplicationController
  before_action :set_fighter, only: [:show, :edit, :update, :destroy]

  def index
    @fighters = Fighter.search(params[:query])
  end

  def show
  end

  def new
    @fighter = Fighter.new
    respond_to do |format|
      format.html
      format.js { render 'show_form' }
    end
  end

  def edit
    respond_to do |format|
      format.html
      format.js { render 'show_form' }
    end
  end

  def create
    @fighter = Fighter.new(fighter_params)

    respond_to do |format|
      if @fighter.save
        format.html { redirect_to @fighter, notice: 'Fighter was successfully created.' }
        format.js
      else
        format.html { render :new }
        format.js { render :new }
      end
    end
  end

  # def create
  #   @fighter = Fighter.new(fighter_params)
  #
  #   respond_to do |format|
  #     if @fighter.save
  #       format.html { redirect_to @fighter, notice: 'Fighter was successfully created.' }
  #       format.js
  #     else
  #       format.html { render :new }
  #       format.js { render :new }
  #     end
  #   end
  # end

  def update
    respond_to do |format|
      if @fighter.update(fighter_params)
        format.html { redirect_to @fighter, notice: 'Fighter was successfully updated.' }
        format.js
      else
        format.html { render :edit }
        format.js { render :edit }
      end
    end
  end

  def destroy
    @fighter.destroy
    respond_to do |format|
      format.html { redirect_to fighters_url, notice: 'Fighter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_fighter
    @fighter = Fighter.find(params[:id])
  end

  def fighter_params
    params.require(:fighter).permit(:first_name, :last_name, :alias, :sport_kind)
  end
end
