class FightsController < ApplicationController
  before_action :set_fight, only: [:show, :edit, :update, :destroy]

  def index
    @fights = Fight.all
  end

  def show
  end

  def new
    @event = Event.find params[:event_id]
    @fight = @event.fights.build
  end

  def edit
  end

  def create
    @event = Event.find params[:event_id]
    @fight = @event.fights.build fight_params

    respond_to do |format|
      if @fight.save
        format.js
      else
        format.js { render 'fights/new' }
      end
    end
  end

  def update
    respond_to do |format|
      if @fight.update(fight_params)
        format.html { redirect_to @fight, notice: 'Fight was successfully updated.' }
        format.json { render :show, status: :ok, location: @fight }
      else
        format.html { render :edit }
        format.json { render json: @fight.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @fight.destroy
    respond_to do |format|
      format.html { redirect_to fights_url, notice: 'Fight was successfully destroyed.' }
      format.js
    end
  end

  private

  def set_fight
    @fight = Fight.find(params[:id])
  end

  def fight_params
    params.require(:fight).permit(:event_id, :fighter1_id, :fighter2_id)
  end

end
