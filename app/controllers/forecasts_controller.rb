class ForecastsController < ApplicationController
  respond_to :js
  decorates_assigned :forecast
  before_action :set_forecast, only: %i[update destroy]

  def create
    fight = Fight.find params[:fight_id]
    @forecast = current_user.forecasts.for(fight).build forecast_params
    @forecast.save
    respond_with @forecast
  end

  def update
    @forecast.update forecast_params
    respond_with @forecast
  end

  def destroy
    @forecast.destroy
  end

  private

  def set_forecast
    @forecast = Forecast.find params[:id]
  end

  def forecast_params
    params.require(:forecast).permit(:winner_id, :way, :round)
  end
end
