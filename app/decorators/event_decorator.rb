class EventDecorator < Draper::Decorator
  delegate_all
  decorates_associations :fights, :forecasts

  def kind
    Event.human_attribute_name "kind.#{object.kind}"
  end

  def status
    Event.human_attribute_name "status.#{object.status}"
  end

  def datetime
    I18n.l object.time, format: :full
  end

  def date
    I18n.l object.time, format: :full_date
  end

  def user_forecasts
    object.forecasts.by(h.current_user).decorate
  end

end
