class FightDecorator < Draper::Decorator
  delegate_all
  decorates_associations :fighter1, :fighter2

  def fighter_tag(fighter_num)
    attribute_name = "fighter#{fighter_num}"
    fighter = object.send(attribute_name).decorate
    fighter.tag class: attribute_name
  end

  def link_to_remove
    h.link_to h.icon(:remove), object, data: {confirm: I18n.t('helpers.confirmation')}, method: :delete,
              class: 'text-danger', remote: true
  end
end
