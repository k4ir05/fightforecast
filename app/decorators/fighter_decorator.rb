class FighterDecorator < Draper::Decorator
  delegate_all

  def link
    h.link_to object.name, object
  end

  def tag(options=nil)
    options ||= {}
    options.deep_merge! data: {id: id}
    options[:class] ||= ''
    options[:class] << ' fighter'
    h.content_tag :span, link, options
  end

end
