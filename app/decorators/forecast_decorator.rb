class ForecastDecorator < FightDecorator
  delegate_all
  decorates_association :fight

  def fighter_tag(fighter_num)
    attribute_name = "fighter#{fighter_num}"
    fighter = fight.send(attribute_name).decorate
    fighter.tag class: "#{attribute_name} #{'winner' if fighter == forecast.winner}"
  end

  def link_to_remove
    if object.persisted?
      h.content_tag :span, class: 'pull-right' do
        h.link_to h.icon(:remove), object, data: {confirm: I18n.t('helpers.confirmation')}, method: :delete,
                  class: 'text-danger', remote: true
      end
    end
  end

  def forecast
    @forecast ||= h.current_user.forecasts.for(object).first_or_initialize
  end
end
