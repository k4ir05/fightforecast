class UserDecorator < Draper::Decorator
  delegate_all

  def name
    object.username
  end

end
