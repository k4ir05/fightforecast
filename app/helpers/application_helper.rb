module ApplicationHelper

  def title(addition = nil)
    base_title = "#{t('application_name')}"
    addition.present? ? "#{base_title} - #{addition}" : base_title
  end

  def close_modal_button
    content_tag(:button, type: 'butotn', class: 'close', data: {dismiss: 'modal'}) do
      content_tag(:span, icon(:times), aria: {hidden: true})
    end
  end

end
