module EventHelper

  def event_kind_collection_for_select
    Event.kinds.keys.map { |type| [Event.human_attribute_name("type.#{type}"), type] }
  end

end
