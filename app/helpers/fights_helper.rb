module FightsHelper

  def fighters_collection(fight)
    fighter1, fighter2 = fight.fighter1, fight.fighter2
    [[fighter1.name, fighter1.id], [fighter2.name, fighter2.id]]
  end

  def win_ways_collection
    FightResult::WAYS_COMMON.collect { |way, value| [way.to_s.humanize, value] }
  end

end
