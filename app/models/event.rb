class Event < ActiveRecord::Base
  belongs_to :author, class_name: 'User'
  has_many :fights
  has_many :forecasts, through: :fights
  validates_presence_of :name, :time, :kind
  enum kind: %i[boxing mma]
  enum status: %i[active finished]

  def upcoming?
    time > Time.current
  end
end
