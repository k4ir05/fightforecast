class Fight < ActiveRecord::Base
  belongs_to :event, required: true
  belongs_to :fighter1, class_name: 'Fighter', required: true
  belongs_to :fighter2, class_name: 'Fighter', required: true
  has_many :forecasts, dependent: :restrict_with_error
  delegate :upcoming?, to: :event
end
