class Fighter < ActiveRecord::Base
  validates_presence_of :first_name, :last_name
  enum sport_kind: %i[boxing mma]

  scope :search, ->(query) {
    where 'first_name ILIKE :q OR last_name ILIKE :q OR alias ILIKE :q', q: "%#{query || ''}%"
  }

  # def self.search(query)
  #   # fighters = Fighter.all
  #
  #   # if query.present?
  #   #
  #   # end
  #
  #   # fighters
  # end

  def name
    [first_name, last_name].compact.join(' ')
  end

  def presentation
    [first_name, "\"#{self.alias}\"", last_name].compact.join(' ')
  end

end
