class Forecast < ActiveRecord::Base
  scope :for, ->(fight) { where fight: fight }
  scope :by, ->(forecaster) { where forecaster: forecaster }

  belongs_to :forecaster, class_name: 'User', required: true
  belongs_to :fight, required: true
  belongs_to :winner, class_name: 'Fighter', required: true
  delegate :upcoming?, to: :fight
  composed_of :fight_result
  validates_uniqueness_of :forecaster_id, scope: :fight_id
end
