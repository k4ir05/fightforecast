class User < ActiveRecord::Base
  attr_accessor :login
  has_many :forecasts, foreign_key: :forecaster_id
  validates :username, presence: true
  validates :username, uniqueness: {case_sensitive: false}, length: {minimum: 2, maximum: 50}, format: {with: /\A[a-zA-Zа-яА-Я0-9\.\/_-]+\z/}

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :timeoutable

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if (login = conditions.delete(:login)).present?
      where(conditions.to_h).where(['lower(username) = :value OR lower(email) = :value', {value: login.mb_chars.downcase.to_s}]).first
    else
      where(conditions.to_h).first
    end
  end

  def name
    username
  end

end
