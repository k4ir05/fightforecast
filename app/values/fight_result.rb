class FightResult
  attr_reader :way, :round
  WAYS = {ud: 0, sd: 1, md: 2, draw: 3, ko: 4, tko: 5, rtd: 6, nc: 7, sub: 8}
  WAYS_COMMON = {decision: 0, ko: 1, sub: 2, draw: 3}

  def initialize(way, round)
    @way, @round = way, round
  end

  def decision?
    way.in? 0..2
  end

  def any_ko?
    way.in? 4..6
  end

  def draw?
    way == 3
  end

  def similar_way?(other_result)
    decision? && other_result.decision? ||
      any_ko? && other_result.any_ko? ||
      draw? && other_result.draw?
  end

end