Rails.application.routes.draw do
  root 'home#index'
  devise_for :users
  resources :fighters
  resources :events, shallow: true do
    resources :fights
  end
  resources :fights, shallow: true do
    resources :forecasts, only: %i[create update destroy]
  end
end
