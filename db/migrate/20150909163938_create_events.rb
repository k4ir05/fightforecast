class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.references :author, null: false, index: true
      t.string :name, null: false, index: true
      t.datetime :time, null: false, index: true
      t.text :description
      t.integer :kind, null: false, default: 0, index: true
      t.integer :status, null: false, default: 0, index: true

      t.timestamps null: false
    end
    add_foreign_key :events, :users, column: :author_id
  end
end
