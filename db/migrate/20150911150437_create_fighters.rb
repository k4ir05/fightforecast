class CreateFighters < ActiveRecord::Migration
  def change
    create_table :fighters do |t|
      t.string :first_name, null: false, index: true
      t.string :last_name, null: false, index: true
      t.string :alias, index: true
      t.integer :sport_kind, null: false, default: 0, index: true

      t.timestamps null: false
    end
  end
end
