class CreateFights < ActiveRecord::Migration
  def change
    create_table :fights do |t|
      t.references :event, index: true, foreign_key: true
      t.references :fighter1, index: true
      t.references :fighter2, index: true

      t.timestamps null: false
    end
    add_foreign_key :fights, :fighters, column: :fighter1_id
    add_foreign_key :fights, :fighters, column: :fighter2_id
  end
end
