class CreateForecasts < ActiveRecord::Migration
  def change
    create_table :forecasts do |t|
      t.references :forecaster, null: false, index: true
      t.references :fight, null: false, index: true, foreign_key: true
      t.references :winner, null: false, index: true
      t.integer :way, null: false, default: 0
      t.integer :round, null: false, default: 1

      t.timestamps null: false
    end
    add_foreign_key :forecasts, :users, column: :forecaster_id
    add_foreign_key :forecasts, :fighters, column: :winner_id
  end
end
