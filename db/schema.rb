# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150927135829) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "events", force: :cascade do |t|
    t.integer  "author_id",               null: false
    t.string   "name",                    null: false
    t.datetime "time",                    null: false
    t.text     "description"
    t.integer  "kind",        default: 0, null: false
    t.integer  "status",      default: 0, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "events", ["author_id"], name: "index_events_on_author_id", using: :btree
  add_index "events", ["kind"], name: "index_events_on_kind", using: :btree
  add_index "events", ["name"], name: "index_events_on_name", using: :btree
  add_index "events", ["status"], name: "index_events_on_status", using: :btree
  add_index "events", ["time"], name: "index_events_on_time", using: :btree

  create_table "fighters", force: :cascade do |t|
    t.string   "first_name",             null: false
    t.string   "last_name",              null: false
    t.string   "alias"
    t.integer  "sport_kind", default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "fighters", ["alias"], name: "index_fighters_on_alias", using: :btree
  add_index "fighters", ["first_name"], name: "index_fighters_on_first_name", using: :btree
  add_index "fighters", ["last_name"], name: "index_fighters_on_last_name", using: :btree
  add_index "fighters", ["sport_kind"], name: "index_fighters_on_sport_kind", using: :btree

  create_table "fights", force: :cascade do |t|
    t.integer  "event_id"
    t.integer  "fighter1_id"
    t.integer  "fighter2_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "fights", ["event_id"], name: "index_fights_on_event_id", using: :btree
  add_index "fights", ["fighter1_id"], name: "index_fights_on_fighter1_id", using: :btree
  add_index "fights", ["fighter2_id"], name: "index_fights_on_fighter2_id", using: :btree

  create_table "forecasts", force: :cascade do |t|
    t.integer  "forecaster_id",             null: false
    t.integer  "fight_id",                  null: false
    t.integer  "winner_id",                 null: false
    t.integer  "way",           default: 0, null: false
    t.integer  "round",         default: 1, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "forecasts", ["fight_id"], name: "index_forecasts_on_fight_id", using: :btree
  add_index "forecasts", ["forecaster_id"], name: "index_forecasts_on_forecaster_id", using: :btree
  add_index "forecasts", ["winner_id"], name: "index_forecasts_on_winner_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "username",               default: "", null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "events", "users", column: "author_id"
  add_foreign_key "fights", "events"
  add_foreign_key "fights", "fighters", column: "fighter1_id"
  add_foreign_key "fights", "fighters", column: "fighter2_id"
  add_foreign_key "forecasts", "fighters", column: "winner_id"
  add_foreign_key "forecasts", "fights"
  add_foreign_key "forecasts", "users", column: "forecaster_id"
end
